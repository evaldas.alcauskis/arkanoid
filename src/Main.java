import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        int width = 700;
        int height = 600;

        JFrame frame = new JFrame("Arkanoid");
        frame.setBounds(0, 0, width, height);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Game game = new Game(width);
        frame.add(game);

        frame.setVisible(true);
    }
}
