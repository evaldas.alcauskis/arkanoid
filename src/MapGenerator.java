import java.awt.*;
import java.util.Random;

public class MapGenerator {
    private int marginTop;
    private int marginSide;

    private Brick[][] map;
    private int aliveBricksCount;

    public MapGenerator(int width, int height, int marginTop, int marginSide, int rows, int cols) {
        this.map = new Brick[rows][cols];
        this.marginTop = marginTop;
        this.marginSide = marginSide;
        int brickWidth = (width - 2 * marginSide) / cols;
        int brickHeight = (height - marginTop) / rows;

        for (int i = 0; i < map.length; i++) {
            for (int u = 0; u < map[0].length; u++) {
                Random generator = new Random();
                int randomInt = generator.nextInt(5);
                this.map[i][u] = new Brick(randomInt, u * brickWidth + marginSide, i * brickHeight + marginTop, brickWidth, brickHeight);
            }
        }

        this.aliveBricksCount = rows * cols;
    }

    public void paint(Graphics2D g) {
        for (int i = 0; i < map.length; i++) {
            for (int u = 0; u < map[0].length; u++) {
                Brick brick = this.map[i][u];
                if (brick.isAlive()) {
                    drawBrick(g, i, u, brick);
                }
            }
        }
    }

    private void drawBrick(Graphics2D g, int i, int u, Brick brick) {
        g.setColor(brick.getColor());
        g.fillRect(u * brick.getWidth() + marginSide, i * brick.getHeight() + marginTop, brick.getWidth(), brick.getHeight());

        g.setStroke(new BasicStroke(3));
        g.setColor(Color.BLACK);
        g.drawRect(u * brick.getWidth() + marginSide, i * brick.getHeight() + marginTop, brick.getWidth(), brick.getHeight());
    }

    public void looseBrickLives(int x, int y, int damage) {
        if (this.map[x][y].looseLives(damage)) {
            aliveBricksCount--;
        }
    }

    public int getAliveBricksCount() {
        return aliveBricksCount;
    }

    public Brick[][] getMap() {
        return map;
    }
}
