import java.awt.*;

public class Player extends Object {
    private static final int step = 20;

    public void prepare() {
        this.x = 310;
        this.y = 550;
        this.width = 110;
        this.height = 8;
    }

    public void paint(Graphics g) {
        g.setColor(Color.GREEN);
        g.fillRect(getX(), getY(), getWidth(), getHeight());
    }

    public void moveToDirection(int direction) {
        this.x += step * direction;
    }
}
