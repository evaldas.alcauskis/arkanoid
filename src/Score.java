public class Score {
    private int step;
    private int total = 0;

    public Score(int step) {
        this.step = step;
    }

    public void plus() {
        total += step;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
