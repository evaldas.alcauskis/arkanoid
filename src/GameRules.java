import java.awt.*;

public class GameRules {
    private static GameRules gameRules = new GameRules();

    private GameRules() {}

    public static GameRules getGameRules(){
        return gameRules;
    }

    public boolean check(Graphics g, Score score, MapGenerator mapGenerator, Ball ball, boolean isPlaying){
        if (mapGenerator.getAliveBricksCount() <= 0) {
            isPlaying = false;
            Text.displayYouWon(g);
        }

        if (ball.getY() > 570) {
            isPlaying = false;
            Text.displayGameOver(g);
        }

        if (mapGenerator.getAliveBricksCount() <= 0 || ball.getY() > 570) {
            Text.displayScoreAndRestartInstructions(g, score);
        }

        return isPlaying;
    }
}
