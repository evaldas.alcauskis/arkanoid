import java.awt.*;

public class Brick extends Object {
    private int lives;

    public Brick(int lives, int x, int y, int width, int height) {
        this.lives = lives;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public boolean isAlive() {
        return lives > 0;
    }

    public boolean looseLives(int lives) {
        boolean becomeDead = false;

        if (this.lives - lives >= 0) {
            if (this.lives - lives == 0) {
                becomeDead = true;
            }
            this.lives -= lives;
        }

        return becomeDead;
    }

    public Color getColor() {
        Color color;

        switch (this.lives) {
            case 4:
                color = Color.RED;
                break;
            case 3:
                color = Color.ORANGE;
                break;
            case 2:
                color = Color.YELLOW;
                break;
            default:
                color = Color.WHITE;
                break;
        }

        return color;
    }
}
