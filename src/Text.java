import java.awt.*;

public class Text {

    public static void displayYouWon(Graphics g) {
        g.setColor(Color.GREEN);
        g.setFont(new Font("Arial", Font.BOLD, 48));
        g.drawString("You won!", 190, 300);
    }

    public static void displayGameOver(Graphics g) {
        g.setColor(Color.RED);
        g.setFont(new Font("Arial", Font.BOLD, 48));
        g.drawString("GAME OVER", 190, 300);
    }

    public static void displayScoreAndRestartInstructions(Graphics g, Score score) {
        g.setFont(new Font("Arial", Font.BOLD, 18));
        g.drawString("Score: " + score.getTotal(), 190, 350);
        g.setFont(new Font("Arial", Font.BOLD, 24));
        g.drawString("Press ENTER to restart", 190, 400);
    }
}
