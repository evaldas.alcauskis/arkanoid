import java.awt.*;

public class Ball extends Object {
    private int directionX;
    private int directionY;

    public Ball(int width) {
        this.width = width;
        this.height = width;
        prepare();
    }

    public void prepare() {
        this.x = 120;
        this.y = 350;
        this.directionX = -8;
        this.directionY = -4;
    }

    public void paint(Graphics g) {
        g.setColor(Color.YELLOW);
        g.fillOval(getX(), getY(), width, height);
    }

    public void move() {
        this.x += directionX;
        this.y += directionY;
    }

    public void changeDirectionX() {
        this.directionX *= -1;
    }

    public void changeDirectionY() {
        this.directionY *= -1;
    }

    public int getDirectionX() {
        return directionX;
    }

    public int getDirectionY() {
        return directionY;
    }
}
