import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

public class Game extends JPanel implements KeyListener, ActionListener {
    private static final int borderWidth = 3;
    private int width;

    private boolean isPlaying = false;

    private Timer timer;
    private Score score;
    private MapGenerator mapGenerator;
    private Ball ball;
    private Player player;

    public Game(int width) {
        this.width = width - 2 * borderWidth;
        this.score = new Score(5);
        prepare();

        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        timer = new Timer(25, this);
        timer.start();
    }

    private void prepare() {
        this.player = new Player();
        this.ball = new Ball(20);

        score.setTotal(0);
        Random generator = new Random();
        int mapRows = generator.nextInt(6) + 4;
        int mapCols = generator.nextInt(10) + 5;

        player.prepare();
        ball.prepare();
        mapGenerator = new MapGenerator(width, 200, 50, 77, mapRows, mapCols);

        repaint();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        // Background
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, 697, 592);

        // Borders
        g.setColor(Color.YELLOW);
        g.fillRect(0, 0, borderWidth, 592);
        g.fillRect(3, 0, 694, 3);
        g.fillRect(697, 0, borderWidth, 592);

        // MapGenerator
        this.mapGenerator.paint((Graphics2D) g);

        // The Player
        this.player.paint(g);

        // The Ball
        this.ball.paint(g);

        // Score
        g.setColor(Color.WHITE);
        g.setFont(new Font("Arial", Font.BOLD, 24));
        g.drawString("" + score.getTotal(), 599, 30);

        isPlaying = GameRules.getGameRules().check(g, score, mapGenerator, ball, isPlaying);

        g.dispose();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        timer.start();

        if (isPlaying) {
            for (int i = 0; i < this.mapGenerator.getMap().length; i++) {
                for (int u = 0; u < this.mapGenerator.getMap()[0].length; u++) {
                    Brick brick = this.mapGenerator.getMap()[i][u];
                    if (brick.isAlive() && ball.getRectangle().intersects(brick.getRectangle())) {
                        this.mapGenerator.looseBrickLives(i, u, 1);
                        score.plus();

                        if (ball.getX() + 10 <= brick.x || ball.getX() + 1 >= brick.x + brick.width) {
                            ball.changeDirectionX();
                        } else {
                            ball.changeDirectionY();
                        }

                        break;
                    }
                }
            }

            if (ball.getX() + ball.getDirectionX() < borderWidth || ball.getX() + ball.getDirectionX() >= (borderWidth + width - ball.getWidth() / 2)) {
                ball.changeDirectionX();
            }
            if (ball.getY() + ball.getDirectionY() <= borderWidth || ball.getRectangle().intersects(player.getRectangle())) {
                ball.changeDirectionY();
            }

            ball.move();
        }

        repaint();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (!isPlaying && (e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_RIGHT)) {
            isPlaying = true;
        }

        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            if (player.getX() <= borderWidth) {
                player.setX(borderWidth);
            } else {
                player.moveToDirection(-1);
            }
        }

        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            if (player.getX() + player.getWidth() >= borderWidth + width) {
                player.setX(borderWidth + width - player.getWidth());
            } else {
                player.moveToDirection(1);
            }
        }

        if (e.getKeyCode() == KeyEvent.VK_ENTER && !isPlaying) {
            prepare();
            isPlaying = true;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
