import org.junit.Assert;
import org.junit.Test;

public class BrickTest {
    @Test
    public void looseLivesAndBeAlive() {
        Brick brick = new Brick(2, 0, 0, 10, 10);
        boolean hasBecomeDead = brick.looseLives(1);

        Assert.assertFalse(hasBecomeDead);
    }

    @Test
    public void looseLivesAndBecomeDead() {
        Brick brick = new Brick(2, 0, 0, 10, 10);
        boolean hasBecomeDead = brick.looseLives(2);

        Assert.assertTrue(hasBecomeDead);
    }
}